Part of `BharatMOOC code`__.

__ http://code.bharatmooc.org/

EdX Repository Skeleton
=======================

A skeleton that can be used as the basis for open-source BharatMOOC repositories.

The ``README.rst`` file, should start with a brief description of the repository
which sets it in the context of other repositories under the ``bharatmooc``
organization. It should make clear where this fits in to the overall BharatMOOC
codebase.

Overview (please modify)
------------------------

The ``README.rst`` file should then provide an overview of the code in this
repository, what the main components are and useful entry points for starting
to understand the code in more detail.


Getting Started (please modify)
-------------------------------

The ``README.rst`` file should include a quick guide to getting up and running.
This should be the simplest set of steps just to show that things are working.

Documentation (please modify)
-----------------------------

The docs for XXX is on Read The Docs:  https://XXX.readthedocs.org

License
-------

The code in this repository is licensed under version 3 of the AGPL unless
otherwise noted.

Please see ``LICENSE.txt`` for details.

How To Contribute
-----------------

Contributions are very welcome.

Please read `How To Contribute <https://github.com/bharatmooc/bharatmooc-platform/blob/master/CONTRIBUTING.rst>`_ for details.

Even though it was written with ``bharatmooc-platform`` in mind, the guidelines
should be followed for Open BharatMOOC code in general.

Reporting Security Issues
-------------------------

Please do not report security issues in public. Please email security@bharatmooc.org

Mailing List and IRC Channel
----------------------------

You can discuss this code on the `bharatmooc-code Google Group`__ or in the
``bharatmooc-code`` IRC channel on Freenode.

__ https://groups.google.com/forum/#!forum/bharatmooc-code
